public interface IHealthServices
{
    float TakeDamage(float damage, float lifeActual, float lifeMax);
}