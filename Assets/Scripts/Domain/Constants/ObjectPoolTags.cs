﻿public class ObjectPoolTags
{
    public const string OBJECT_POOL_SKELETON_ENEMY = "SKELETON_ENEMY";
    public const string OBJECT_POOL_TOWER_BALLESTA = "TOWER_BALLESTA";
    public const string OBJECT_POOL_STRUCTURE_MURAL = "STRUCTURE_MURAL";
    public const string OBJECT_POOL_ENEMY_DEATH_PS = "ENEMY_DEATH_PS";
}